# Copyright (c) 2021 Huawei Device Co., Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import("//build/gn/fangtian.gni")

common_source = [
  "src/fs-poll.c",
  "src/idna.c",
  "src/inet.c",
  "src/random.c",
  "src/strscpy.c",
  "src/threadpool.c",
  "src/timer.c",
  "src/uv-common.c",
  "src/uv-data-getter-setters.c",
  "src/version.c",
]

nonwin_srcs = [
    "src/unix/epoll.c",
    "src/unix/async.c",
    "src/unix/core.c",
    "src/unix/dl.c",
    "src/unix/fs.c",
    "src/unix/getaddrinfo.c",
    "src/unix/getnameinfo.c",
    "src/unix/loop.c",
    "src/unix/loop-watcher.c",
    "src/unix/pipe.c",
    "src/unix/poll.c",
    "src/unix/process.c",
    "src/unix/random-devurandom.c",
    "src/unix/signal.c",
    "src/unix/stream.c",
    "src/unix/tcp.c",
    "src/unix/thread.c",
    "src/unix/tty.c",
    "src/unix/udp.c",
  ]

# This is the configuration needed to use libuv.
config("libuv_config") {
  include_dirs = [
    "include",
    "src",
    "src/unix",
  ]
  cflags = [ "-Wno-unused-parameter" ]
  cflags += [
    "-Wno-incompatible-pointer-types",
    "-D_GNU_SOURCE",
    "-D_POSIX_C_SOURCE=200112",
  ]

  # Adding NDEBUG macro manually to avoid compilation
  # error in debug version, FIX ME
  # https://gitee.com/openharmony/build/pulls/1206/files
  defines = [ "NDEBUG" ]
}

# This is the configuration used to build libuv itself.
# It should not be needed outside of this library.
config("libuv_private_config") {
  visibility = [ ":*" ]
  include_dirs = [
    "include",
    "src",
    "src/unix",
  ]

  # Adding NDEBUG macro manually to avoid compilation
  # error in debug version, FIX ME
  # https://gitee.com/openharmony/build/pulls/1206/files
  defines = [ "NDEBUG" ]
}

ft_source_set("libuv_source") {
  configs = [ ":libuv_config" ]
  sources = common_source

  # if (is_ohos || (defined(is_android) && is_android)) {
  #   sources += nonwin_srcs + [
  #                 "src/unix/linux-core.c",
  #                 "src/unix/linux-inotify.c",
  #                 "src/unix/linux-syscalls.c",
  #                 "src/unix/procfs-exepath.c",
  #                 "src/unix/pthread-fixes.c",
  #                 "src/unix/random-getentropy.c",
  #                 "src/unix/random-getrandom.c",
  #                 "src/unix/random-sysctl-linux.c",
  #                 "src/unix/proctitle.c",
  #               ]
  # } else if (is_linux) {
  #   sources += nonwin_srcs + [
  #                 "src/unix/linux-core.c",
  #                 "src/unix/linux-inotify.c",
  #                 "src/unix/linux-syscalls.c",
  #                 "src/unix/procfs-exepath.c",
  #                 "src/unix/random-getrandom.c",
  #                 "src/unix/random-sysctl-linux.c",
  #                 "src/unix/proctitle.c",
  #               ]
  # }
  sources += nonwin_srcs + [
                  "src/unix/linux-core.c",
                  "src/unix/linux-inotify.c",
                  "src/unix/linux-syscalls.c",
                  "src/unix/procfs-exepath.c",
                  "src/unix/random-getrandom.c",
                  "src/unix/random-sysctl-linux.c",
                  "src/unix/proctitle.c",
                ]

  subsystem_name = "thirdparty"
  part_name = "libuv"
}

ft_static_library("uv_static") {
  deps = [ ":libuv_source" ]
  public_configs = [ ":libuv_config" ]
  subsystem_name = "thirdparty"
  part_name = "libuv"
}

ft_shared_library("uv") {
  deps = [ ":libuv_source" ]
  public_configs = [ ":libuv_config" ]
  subsystem_name = "thirdparty"
  part_name = "libuv"

  # if (is_ohos) {
  #   output_extension = "so"
  # }
  output_extension = "so"
  
  # install_images = [
  #   "system",
  #   "updater",
  # ]
}

# ohos_ndk_library("libuv_ndk") {
#   ndk_description_file = "./libuv.ndk.json"
#   min_compact_version = "1"
#   output_name = "uv"
#   output_extension = "so"
# }

# ohos_ndk_headers("libuv_header") {
#   dest_dir = "$ndk_headers_out_dir"
#   sources = [ "include/uv_ndk/uv.h" ]
# }

# ohos_ndk_headers("libuv_uv_header") {
#   dest_dir = "$ndk_headers_out_dir/uv"
#   sources = [
#     "include/uv/aix.h",
#     "include/uv/bsd.h",
#     "include/uv/darwin.h",
#     "include/uv/errno.h",
#     "include/uv/linux.h",
#     "include/uv/os390.h",
#     "include/uv/posix.h",
#     "include/uv/stdint-msvc2008.h",
#     "include/uv/sunos.h",
#     "include/uv/threadpool.h",
#     "include/uv/tree.h",
#     "include/uv/unix.h",
#     "include/uv/version.h",
#     "include/uv/win.h",
#   ]
# }
